# App Core 

The package handles the core projects


## Getting started

The features that have been developed and ready to be used by other modules are stated below with some basic information

### 1. Dependency Manager

This package contains all the third party libraries needed by all the flutter project. In order to use this package, refer to [Readme](app_dependency_manager/README.md)


### 2. Generic Linting Manager

This package contains brief rules set to be used by all domains and to maintain the same linting rules throughout the flutter development process. In order to use this package, refer to [Readme](app_lint/README.md)


## Under Development / Discussion
1. Common Logging Service [Simmilar to Logger package]
2. Api Service Manager using Dio
3. Common Firebase configuration with option to distinguish between modules
4. Pigeon support


## Changelog
You can refer to the following document for changelog: [CHANGELOG](CHANGELOG.md)
