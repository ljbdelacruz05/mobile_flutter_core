library app_dependency_manager;


export 'package:auto_size_text/auto_size_text.dart';
export 'package:bloc_test/bloc_test.dart';
export 'package:cached_network_image/cached_network_image.dart';
export 'package:confetti/confetti.dart';
export 'package:dartz/dartz.dart';
export 'package:decimal/decimal.dart';
export 'package:dio/dio.dart';
export 'package:equatable/equatable.dart';
export 'package:fl_chart/fl_chart.dart';
export 'package:flutter_bloc/flutter_bloc.dart';
export 'package:flutter_portal/flutter_portal.dart';
export 'package:flutter_slidable/flutter_slidable.dart';
export 'package:flutter_svg/flutter_svg.dart';
export 'package:freezed_annotation/freezed_annotation.dart';
export 'package:go_router/go_router.dart';
export 'package:grouped_list/grouped_list.dart';
export 'package:http_certificate_pinning/http_certificate_pinning.dart';
export 'package:json_annotation/json_annotation.dart';
export 'package:lottie/lottie.dart';
export 'package:provider/provider.dart';
export 'package:readmore/readmore.dart';
export 'package:shared_preferences/shared_preferences.dart';
export 'package:simple_connection_checker/simple_connection_checker.dart';
export 'package:sms_autofill/sms_autofill.dart';
export 'package:theme_provider/theme_provider.dart';
export 'package:uuid/uuid.dart';
export 'package:visibility_detector/visibility_detector.dart';
export 'package:get_it/get_it.dart';
export 'dart:async';
export 'package:hive/hive.dart';


// UI
export 'package:calendar_date_picker2/calendar_date_picker2.dart';
export 'package:carousel_slider/carousel_controller.dart';
export 'package:carousel_slider/carousel_options.dart';
export 'package:carousel_slider/carousel_slider.dart';
export 'package:shimmer/shimmer.dart';
export 'package:awesome_notifications/awesome_notifications.dart';
export 'package:awesome_notifications/awesome_notifications_web.dart';

// maps
export 'package:geolocator/geolocator.dart';

