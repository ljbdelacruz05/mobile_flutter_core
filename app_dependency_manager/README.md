# App Dependency Manager

The package handles the core dependecies to be utilised by all the domains.

## How to use

In order to use this package, you need to do the following:

1. Remove all third party dependencies from your module
2. Add the following dependency in your module as follows:

```
  app_dependency_manager:
    git:
      url: git@gitlab.com:ljbdelacruz05/mobile_flutter_core.git 
      ref: master
      path: app_dependency_manager
```



## Getting started

The features that have been developed and ready to be used by other modules are stated below with some basic information

## How to update Dependency Manager

Any third party libraries that you wish to use should be added in this repo's **pubsec.yaml** file. Once added, you need to add export statement of that package in **app\_dependency\_manager.dart** file

```dart
export 'package:flutter_bloc/flutter_bloc.dart';
```


## How to use in your project 

if dependencies already included just use 
import 'package:app_dependency_manager/app_dependency_manager.dart';
no need to manually import each dependencies your app is using whenever you you use bloc, dio and etc