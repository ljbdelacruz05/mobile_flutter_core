# App Lint

The package handles the generic rules set for linting to be used by all projects

## How to use

In order to use this package, you need to do the following:

1. Add the following dev_dependency in your module as follows:

```
  app_lint:
    git:
      url: git@gitlab.com:ljbdelacruz05/mobile_flutter_core.git
      ref: master
      path: app_lint
```

2. In your analysis_options.yaml file (Create it if not present already), remove everything and add the following line only:

```
  include: package:app_lint/analysis_options.yaml
```

